import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/routes/routes.dart';
import 'package:nd_app/app/widgets/splash_screen.dart';
import 'package:workmanager/workmanager.dart';

final ThemeData appThemeData = ThemeData(
  primaryColor: Color.fromRGBO(43, 186, 217, 1),
  primaryColorLight: Color.fromRGBO(48, 187, 242, 1),
  accentColor: Color.fromRGBO(105, 166, 15, 1),
  cardColor: Color.fromRGBO(242, 242, 242, 1),
  fontFamily: 'Raleway',
);

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Workmanager.initialize(callbackDispatcher, isInDebugMode: false);
// Periodic task registration
  Workmanager.registerPeriodicTask(
    '2',
    'workdays',
    frequency: Duration(minutes: 5),
  );

  runApp(
    GetMaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        theme: appThemeData,
        defaultTransition: Transition.fadeIn,
        opaqueRoute: Get.isOpaqueRouteDefault,
        popGesture: Get.isPopGestureEnable,
        getPages: AppPages.pages,
        home: SplashScreen()),
  );
}

void callbackDispatcher() {
  Workmanager.executeTask((task, inputData) {
    // initialise the plugin of flutterlocalnotifications.
    FlutterLocalNotificationsPlugin flip = FlutterLocalNotificationsPlugin();

    // app_icon needs to be a added as a drawable
    // resource to the Android head project.
    final android = AndroidInitializationSettings('@mipmap/ic_launcher');
    final iOs = IOSInitializationSettings();

    // initialise settings for both Android and iOS device.
    final settings = InitializationSettings(android, iOs);
    flip.initialize(settings);
    _showNotificationWithDefaultSound(flip);
    return Future.value(true);
  });
}

Future _showNotificationWithDefaultSound(flip) async {
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'your channel id', 'your channel name', 'your channel description',
      importance: Importance.Max, priority: Priority.High);
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();

  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flip.show(
    0,
    'Registra tu tiempo',
    'Recuerda regtistrar tus actividades',
    platformChannelSpecifics,
    payload: 'Default_Sound',
  );
}
