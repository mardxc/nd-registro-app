class UrlApi {
  String _urlApi = 'https://nd-registro.herokuapp.com/api/';
  String _urlRegiter = 'auth/signup';
  String _urlAuth = 'auth/signin';
  String _urlMe = 'users/me';
  String _urlCheckin = 'workday/checkin';
  String _urlWorkday = 'workday';
  String _urlChecks = 'workday/checks';
  String _urlTask = 'task/user';
  String _urlTaskCreate = 'task';
  String _urlTaskUpdate = 'task/update';
  String _urlProject = 'project';
  String _urlPhase = 'phase';
  String _urlStatus = 'status';
  String _urlActivity = 'activity';
  String _urlDelay = 'delay';
  String _urlCriteria = 'criteria';
  String _urlUsers = 'users';

  String get urlApi => _urlApi;
  String get urlAuth => _urlApi + _urlAuth;
  String get urlRegiter => _urlApi + _urlRegiter;
  String get urlMe => _urlApi + _urlMe;
  String get urlCheckin => _urlApi + _urlCheckin;
  String get urlWorkday => _urlApi + _urlWorkday;
  String get urlChecks => _urlApi + _urlChecks;
  String get urlTask => _urlApi + _urlTask;
  String get urlTaskCreate => _urlApi + _urlTaskCreate;
  String get urlTaskUpdate => _urlApi + _urlTaskUpdate;
  String get urlProject => _urlApi + _urlProject;
  String get urlPhase => _urlApi + _urlPhase;
  String get urlStatus => _urlApi + _urlStatus;
  String get urlActivity => _urlApi + _urlActivity;
  String get urlDelay => _urlApi + _urlDelay;
  String get urlCriteria => _urlApi + _urlCriteria;
  String get urlUsers => _urlApi + _urlUsers;
}
