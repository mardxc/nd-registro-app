import 'package:nd_app/app/modules/home/home_page.dart';
import 'package:nd_app/app/modules/login/login_page.dart';
import 'package:nd_app/app/modules/signup/signup_page.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/modules/task/add_task/add_task_page.dart';
import 'package:nd_app/app/modules/task/assign_task/assign_task_page.dart';
import 'package:nd_app/app/modules/task/task_page.dart';
import 'package:nd_app/app/modules/task/update_task/update_task_page.dart';
import 'package:nd_app/app/widgets/splash_screen.dart';

abstract class AppPages {
  static final pages = [
    GetPage(name: '/splash', page: () => SplashScreen()),
    GetPage(name: '/home', page: () => HomePage()),
    GetPage(name: '/login', page: () => LoginPage()),
    GetPage(name: '/signup', page: () => SignupPage()),
    GetPage(name: '/task', page: () => TaskPage()),
    GetPage(name: '/add-task', page: () => AddTaskPage()),
    GetPage(name: '/assign-task', page: () => AssignTaskPage()),
    GetPage(name: '/update-task/:taskId', page: () => UpdateTaskPage()),
  ];
}
