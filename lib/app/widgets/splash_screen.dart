import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/modules/auth_controller.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(
      init: AuthController(),
      builder: (_) => Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: Color.fromRGBO(0, 29, 47, 1),
        ),
        child: Image.asset(
          'assets/img/loading-screeen.gif',
          fit: BoxFit.scaleDown,
        ),
      ),
    );
  }
}
