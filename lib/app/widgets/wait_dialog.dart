import 'package:flutter/material.dart';

class WaitDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      scrollable: true,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 10.0,
          ),
          Image.asset('assets/img/loading.gif'),
          SizedBox(
            height: 20.0,
          ),
        ],
      ),
    );
  }
}
