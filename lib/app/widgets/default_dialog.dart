import 'package:flutter/material.dart';

class DefaultMessage extends StatelessWidget {
  final titulo;
  final mensaje;
  final popped;
  final VoidCallback onPopped;
  DefaultMessage({
    this.titulo,
    this.mensaje,
    this.popped = true,
    this.onPopped,
  });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('$titulo'),
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            Text('$mensaje'),
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: () {
            popped ? Navigator.of(context).pop() : print('$popped');
            onPopped?.call();
          },
          child: Text(
            'OK',
            style: TextStyle(
              color: Colors.blue,
            ),
          ),
        ),
      ],
    );
  }
}
