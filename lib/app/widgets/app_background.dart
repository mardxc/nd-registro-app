import 'package:flutter/material.dart';

class AppBackground extends StatelessWidget {
  final Widget child;
  AppBackground({this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/img/background/login-background.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: child, //_contenido(context),
    );
  }
}
