import 'package:intl/intl.dart';

class Validate {
  String convertirFechaToHora(String strDate) {
    var fecha;
    if (strDate != null && strDate != 'NA' && strDate != 'null') {
      DateTime todayDate = DateTime.parse(strDate);
      fecha = DateFormat('h:mm a').format(todayDate) ?? 'NA';
    } else {
      fecha = 'NA';
    }
    return fecha;
  }

  String convertirIsoToFecha(String strDate) {
    var fecha;
    if (strDate != null && strDate != 'NA' && strDate != 'null') {
      DateTime todayDate = DateTime.parse(strDate);
      fecha = DateFormat('yyyy-M-d').format(todayDate) ?? 'NA';
    } else {
      fecha = 'NA';
    }
    return fecha;
  }

  String convertirFechaToDia(String strDate) {
    Map<int, String> dias = {
      1: 'Lunes',
      2: 'Martes',
      3: 'Miercoles',
      4: 'Jueves',
      5: 'Viernes',
      6: 'Sabado',
      7: 'Domingo'
    };
    String fecha;
    if (strDate != null && strDate != 'NA' && strDate != 'null') {
      DateTime todayDate = DateTime.parse(strDate);
      int numDia = todayDate.weekday;
      fecha = dias[numDia];
    } else {
      fecha = 'NA';
    }
    return fecha;
  }

  String cambiarFormatoFecha(String strDate) {
    var fecha;
    if (strDate != 'null' && strDate != null && strDate != 'NA') {
      DateTime todayDate = DateTime.parse(strDate);
      fecha = DateFormat('d-M-yyyy').format(todayDate);
    } else {
      fecha = 'NA';
    }
    return fecha;
  }
}
