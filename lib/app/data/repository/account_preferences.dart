import 'package:shared_preferences/shared_preferences.dart';

class AccountPreferences {
  static final AccountPreferences _accounInstancia =
      new AccountPreferences._internal();
  factory AccountPreferences() {
    return _accounInstancia;
  }

  AccountPreferences._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  //get and set//
  get email => _prefs.getString('email') ?? '';
  set email(String value) => _prefs.setString('email', value);

  get token => _prefs.getString('token') ?? '';
  set token(String value) => _prefs.setString('token', value);

  get lastname => _prefs.getString('lastname') ?? '';
  set lastname(String value) => _prefs.setString('lastname', value);

  get name => _prefs.getString('name') ?? '';
  set name(String value) => _prefs.setString('name', value);

  get username => _prefs.getString('username') ?? '';
  set username(String value) => _prefs.setString('username', value);

  get id => _prefs.getString('id') ?? '';
  set id(String value) => _prefs.setString('id', value);

  get check => _prefs.getInt('check') ?? 0;
  set check(int value) => _prefs.setInt('check', value);

  get role => _prefs.getString('role') ?? 0;
  set role(String value) => _prefs.setString('role', value);
}
