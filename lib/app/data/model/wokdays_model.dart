import 'package:meta/meta.dart' show required;

class WorkdaysModel {
  String id;
  String start;
  String location;
  String activity;

  WorkdaysModel({
    @required this.id,
    @required this.start,
    this.location,
    this.activity,
  });

  static WorkdaysModel fromJson(Map<String, dynamic> json) {
    if (json['start'] == null) {
      json['start'] = 'NA';
    }
    if (json['location'] == null) {
      json['location'] = '0, 0';
    }
    if (json['activity']['description'] == null) {
      json['activity']['description'] = 'NA';
    }

    return WorkdaysModel(
      id: json['_id'],
      start: json['start'],
      location: json['location'],
      activity: json['activity']['description'],
    );
  }
}
