import 'package:meta/meta.dart' show required;
import 'package:nd_app/app/utils/validations.dart';

class TaskModel {
  String id;
  String activity;
  String description;
  String start;
  String end;
  int hours;
  String status;
  String project;

  TaskModel({
    @required this.id,
    @required this.activity,
    @required this.description,
    @required this.start,
    @required this.end,
    @required this.hours,
    @required this.status,
    @required this.project,
  });

  static TaskModel fromJson(Map<String, dynamic> json) {
    final Validate _validar = Validate();

    return TaskModel(
      id: json['_id'],
      activity: json['activity'],
      description: json['description'],
      start: _validar.convertirIsoToFecha(json['start']),
      end: _validar.convertirIsoToFecha(json['end']),
      hours: json['hours'],
      status: json['status']['description'] ?? 'NA',
      project: json['project']['name'] ?? 'NA',
    );
  }
}
