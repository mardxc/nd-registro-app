class LocationtModel {
  String lat;
  String lon;
  String type;

  LocationtModel({this.lat, this.lon, this.type});

  LocationtModel.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    lon = json['lon'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lat'] = this.lat;
    data['lon'] = this.lon;
    data['type'] = this.type;
    return data;
  }
}
