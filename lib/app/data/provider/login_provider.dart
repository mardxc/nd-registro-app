import 'dart:convert';

import 'package:nd_app/app/routes/remote_routes.dart';
import 'package:http/http.dart' as http;

class LoginProvider {
  final _url = UrlApi();

  getAccessLogin(body) {
    return http.post(
      Uri.parse(_url.urlAuth),
      headers: {'Content-Type': 'Application/json'},
      body: json.encode(body),
    );
  }
}
