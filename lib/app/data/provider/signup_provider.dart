import 'dart:convert';

import 'package:nd_app/app/routes/remote_routes.dart';
import 'package:http/http.dart' as http;

class SignupProvider {
  final _url = UrlApi();

  doRegister(body) {
    return http.post(
      Uri.parse(_url.urlRegiter),
      headers: {'Content-Type': 'Application/json'},
      body: json.encode(body),
    );
  }
}
