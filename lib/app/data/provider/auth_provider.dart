import 'package:nd_app/app/routes/remote_routes.dart';
import 'package:http/http.dart' as http;

class AuthProvider {
  final _url = UrlApi();

  checkToken(token) {
    return http.post(
      Uri.parse(_url.urlMe),
      headers: {
        'x-access-token': '$token',
      },
    );
  }
}
