import 'dart:convert';

import 'package:nd_app/app/data/model/wokdays_model.dart';
import 'package:nd_app/app/data/repository/account_preferences.dart';
import 'package:nd_app/app/routes/remote_routes.dart';
import 'package:http/http.dart' as http;

class HomeProvider {
  final _url = UrlApi();
  final AccountPreferences _accountPreferences = AccountPreferences();

  Future<List<WorkdaysModel>> getWorkdays(date) async {
    try {
      Map<String, dynamic> body = {
        'userId': _accountPreferences.id,
        'date': date,
      };
      final response = await http.post(Uri.parse(_url.urlChecks),
          headers: {
            'Content-Type': 'Application/json',
            'x-access-token': _accountPreferences.token
          },
          body: json.encode(body));

      Map decoData = json.decode(response.body);

      return (decoData['data'] as List)
          .map((e) => WorkdaysModel.fromJson(e))
          .toList();
    } catch (e) {
      return [];
    }
  }

  checkIn(body) async {
    return await http.post(
      Uri.parse(_url.urlCheckin),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token,
      },
      body: json.encode(body),
    );
  }

  getActivities() async {
    return await http.get(
      Uri.parse('${_url.urlActivity}'),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
    );
  }
}
