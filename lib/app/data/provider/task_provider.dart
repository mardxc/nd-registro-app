import 'dart:convert';
import 'package:nd_app/app/data/model/task_model.dart';
import 'package:nd_app/app/data/repository/account_preferences.dart';
import 'package:nd_app/app/routes/remote_routes.dart';
import 'package:http/http.dart' as http;

class TaskProvider {
  final _url = UrlApi();
  final AccountPreferences _accountPreferences = AccountPreferences();

  Future<List<TaskModel>> getTasks(date) async {
    try {
      Map<String, dynamic> body = {
        'userId': _accountPreferences.id,
        'date': date,
      };
      final response = await http.post(
        Uri.parse(_url.urlTask),
        headers: {
          'Content-Type': 'Application/json',
          'x-access-token': _accountPreferences.token
        },
        body: json.encode(body),
      );

      Map decoData = json.decode(response.body);
      return (decoData['data'] as List)
          .map((e) => TaskModel.fromJson(e))
          .toList();
    } catch (e) {
      return [];
    }
  }

  createTask(body) async {
    return await http.post(
      Uri.parse(_url.urlTaskCreate),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
      body: json.encode(body),
    );
  }

  getTaskById(id) async {
    return await http.get(
      Uri.parse('${_url.urlTaskCreate}/$id'),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
    );
  }

  updateTask(body) async {
    return await http.post(
      Uri.parse(_url.urlTaskUpdate),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
      body: json.encode(body),
    );
  }

  getProjects() async {
    return await http.get(
      Uri.parse('${_url.urlProject}'),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
    );
  }

  getPhases() async {
    return await http.get(
      Uri.parse('${_url.urlPhase}'),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
    );
  }

  getStatus() async {
    return await http.get(
      Uri.parse('${_url.urlStatus}'),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
    );
  }

  getDelays() async {
    return await http.get(
      Uri.parse('${_url.urlDelay}'),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
    );
  }

  getCriteria() async {
    return await http.get(
      Uri.parse('${_url.urlCriteria}'),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
    );
  }

  getUsers() async {
    return await http.get(
      Uri.parse('${_url.urlUsers}'),
      headers: {
        'Content-Type': 'Application/json',
        'x-access-token': _accountPreferences.token
      },
    );
  }
}
