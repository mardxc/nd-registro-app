import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/data/provider/task_provider.dart';
import 'package:nd_app/app/utils/validations.dart';
import 'package:nd_app/app/widgets/default_dialog.dart';

class UpdateTaskController extends GetxController {
  String taskId;

  final TaskProvider _taskProvider = TaskProvider();

  final formAddTask = GlobalKey<FormState>();
  bool _updateForm;
  bool _loading;

  String projectCtrl;
  List dataProject = [
    {
      'display': 'None',
      'value': '0',
    }
  ];
  String delayCtrl;
  List dataDelay = [
    {
      'display': 'None',
      'value': '0',
    }
  ];
  final TextEditingController activityCtrl = TextEditingController();
  final TextEditingController descriptionCtrl = TextEditingController();
  String phaseCtrl;
  List dataPhase = [
    {
      'display': 'None',
      'value': '0',
    }
  ];
  final TextEditingController startCtrl = TextEditingController();
  final TextEditingController endCtrl = TextEditingController();
  final TextEditingController hoursCtrl = TextEditingController();
  final TextEditingController hoursWorkedCtrl = TextEditingController();
  final TextEditingController delayDescCtrl = TextEditingController();
  bool _taskNoPlannedCtrl;

  String statusCtrl;
  List dataStatus = [
    {
      'display': 'None',
      'value': '0',
    }
  ];
  String criteriosCtrl;
  List dataCriterios = [
    {
      'display': 'None',
      'value': '0',
    }
  ];
  Map<String, dynamic> criteriosSelect = {};
  final Validate _validar = Validate();

  bool get taskNoPlannedCtrl => _taskNoPlannedCtrl;
  set taskNoPlannedCtrl(bool val) => this._taskNoPlannedCtrl = val;
  bool get loading => _loading;
  bool get updateForm => _updateForm;
  set updateForm(bool val) => this._updateForm = val;

  @override
  void onInit() {
    taskId = Get.parameters['taskId'];
    _updateForm = false;
    _loading = true;
    _taskNoPlannedCtrl = false;
    _initForm();
    super.onInit();
  }

  _initForm() async {
    try {
      await _getProjects();
      await _getDelays();
      await _getPhases();
      await _getStatus();
      await _getCriterios();
      final response = await _getInfoTask();

      if (response['status']) {
        _loading = false;
        update(['add-task']);
      } else {
        await Get.dialog(
          DefaultMessage(
            titulo: response['type'],
            mensaje: response['msg'],
          ),
        );
        Get.back();
      }
    } catch (e) {
      await Get.dialog(
        DefaultMessage(
          titulo: 'Exception',
          mensaje: e,
        ),
      );
      Get.back();
    }
  }

  void takeCriteria(String key, dynamic value) {
    criteriosSelect[key] = value;
  }

  Future<bool> _getProjects() async {
    final res = await _taskProvider.getProjects();
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);
      for (var item in dataMap['data']) {
        dataProject.add({
          'value': item['_id'],
          'display': item['name'],
        });
      }
    }
    return true;
  }

  Future<bool> _getDelays() async {
    final res = await _taskProvider.getDelays();
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);
      for (var item in dataMap['data']) {
        dataDelay.add({
          'value': item['_id'],
          'display': item['description'],
        });
      }
    }
    return true;
  }

  Future<bool> _getPhases() async {
    final res = await _taskProvider.getPhases();
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);
      for (var item in dataMap['data']) {
        dataPhase.add({
          'value': item['_id'],
          'display': item['description'],
        });
      }
    }
    return true;
  }

  Future<bool> _getStatus() async {
    final res = await _taskProvider.getStatus();
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);
      for (var item in dataMap['data']) {
        dataStatus.add({
          'value': item['_id'],
          'display': item['description'],
        });
      }
    }
    return true;
  }

  Future<bool> _getCriterios() async {
    final res = await _taskProvider.getCriteria();
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);
      dataCriterios.clear();
      for (var item in dataMap['data']) {
        dataCriterios.add({
          'value': item['_id'],
          'display': item['description'],
        });
      }
    }
    return true;
  }

  updateTask() async {
    Map<String, dynamic> body = {
      'projectId': projectCtrl,
      'delayId': delayCtrl,
      'activity': activityCtrl.text,
      'description': descriptionCtrl.text,
      'phase': phaseCtrl,
      'start': startCtrl.text,
      'end': endCtrl.text,
      'hours': hoursCtrl.text,
      'status': statusCtrl,
      'taskId': taskId,
      'delay_desc': delayDescCtrl.text,
      'hours_worked': hoursWorkedCtrl.text,
      'task_noplanned': taskNoPlannedCtrl,
      'criteria': criteriosSelect
    };
    final res = await _taskProvider.updateTask(body);
    Map<String, dynamic> response = {
      'type': 'Error',
      'msg': '',
      'status': false
    };
    if (res.statusCode == 200) {
      response['type'] = 'Success';
      response['status'] = true;
    }
    Map dataMap = json.decode(res.body);
    response['msg'] = dataMap['message'];
    return response;
  }

  _getInfoTask() async {
    Map<String, dynamic> response = {
      'type': 'Error',
      'msg': '',
      'status': false
    };
    final res = await _taskProvider.getTaskById(taskId);
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);

      projectCtrl = dataMap['data']['project'];
      delayCtrl = dataMap['data']['delay'];
      activityCtrl.text = dataMap['data']['activity'];
      descriptionCtrl.text = dataMap['data']['description'];
      phaseCtrl = dataMap['data']['phase'];
      startCtrl.text = _validar.convertirIsoToFecha(dataMap['data']['start']);
      endCtrl.text = _validar.convertirIsoToFecha(dataMap['data']['end']);
      hoursCtrl.text = dataMap['data']['hours'].toString();
      statusCtrl = dataMap['data']['status'];
      delayDescCtrl.text = dataMap['data']['delay_desc'];
      hoursWorkedCtrl.text = dataMap['data']['hours_worked'] == null
          ? ''
          : dataMap['data']['hours_worked'].toString();
      taskNoPlannedCtrl = dataMap['data']['task_noplanned'];
      if (dataMap['data']['criteria'] != null) {
        for (var i = 0; i < dataMap['data']['criteria'].length; i++) {
          final item = dataMap['data']['criteria'][i];
          takeCriteria(item['criteria'], item['description']);
        }
      }
      response['type'] = 'Success';
      response['status'] = true;
    }
    return response;
  }
}
