import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/modules/task/update_task/update_task_controller.dart';
import 'package:nd_app/app/widgets/default_dialog.dart';
import 'package:nd_app/app/widgets/wait_dialog.dart';

class UpdateTaskPage extends StatelessWidget {
  final double h = Get.size.height;
  final double w = Get.size.width;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Actualizar Tarea'),
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.file_present)),
              Tab(icon: Icon(Icons.lock_clock)),
              Tab(icon: Icon(Icons.check)),
            ],
          ),
        ),
        floatingActionButton: _submit(context),
        body: GetBuilder<UpdateTaskController>(
          id: 'add-task',
          init: UpdateTaskController(),
          builder: (_) {
            Widget widget = Container();
            if (_.loading) {
              widget = WaitDialog();
            } else {
              widget = Container(
                margin: EdgeInsets.symmetric(
                  horizontal: w * 0.04,
                  vertical: h * 0.04,
                ),
                width: w * 0.96,
                child: Form(
                  key: _.formAddTask,
                  child: TabBarView(
                    children: [
                      // Generales del proyecto
                      _tabGenerales(context),
                      // Retrasos y criterios de aceptacion
                      _tabDelay(context),
                      // Criterios de aceptacion
                      _tabCriterios(context),
                    ],
                  ),
                ),
              );
            }
            return widget;
          },
        ),
      ),
    );
  }

  Widget _tabGenerales(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _project(context),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(child: _phase(context)),
              Expanded(child: _status(context)),
            ],
          ),
          _activity(context),
          _description(context),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(child: _start(context)),
              Expanded(child: _end(context)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(child: _hours(context)),
              Expanded(child: _hoursWorked(context)),
            ],
          ),
        ],
      ),
    );
  }

  Widget _tabDelay(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _delay(context),
          _delayDes(context),
          _taskPlanned(context),
        ],
      ),
    );
  }

  Widget _tabCriterios(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) {
        return ListView.builder(
          shrinkWrap: true,
          itemCount: _.dataCriterios.length > 1 ? _.dataCriterios.length : 1,
          itemBuilder: (BuildContext context, int index) {
            final _item = _.dataCriterios[index];
            return TextFormField(
              keyboardType: TextInputType.multiline,
              maxLines: 3,
              decoration: InputDecoration(labelText: '${_item['display']}'),
              initialValue: _.criteriosSelect[_item['display']],
              style: TextStyle(
                  fontSize: MediaQuery.of(context).size.height * 0.021),
              onChanged: (val) {
                _.takeCriteria(_item['display'], val);
              },
              validator: (value) {
                if (value.isEmpty) {
                  return 'El campo es requerido';
                }
                return null;
              },
            );
          },
        );
      },
    );
  }

  Widget _project(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => DropDownFormField(
        filled: false,
        titleText: 'Proyecto',
        value: _.projectCtrl,
        onChanged: (value) {
          _.projectCtrl = value;
          _.update(['add-task']);
        },
        hintText: 'Seleciona uno',
        dataSource: _.dataProject,
        textField: 'display',
        valueField: 'value',
      ),
    );
  }

  Widget _delay(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => DropDownFormField(
        filled: false,
        titleText: 'Motivo de Atraso',
        value: _.delayCtrl,
        onChanged: (value) {
          _.delayCtrl = value;
          _.update(['add-task']);
        },
        hintText: 'Seleciona uno',
        dataSource: _.dataDelay,
        textField: 'display',
        valueField: 'value',
      ),
    );
  }

  Widget _activity(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => TextFormField(
        readOnly: true,
        decoration: InputDecoration(labelText: 'Actividad'),
        controller: _.activityCtrl,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _description(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => TextFormField(
        keyboardType: TextInputType.multiline,
        maxLines: 3,
        decoration: InputDecoration(labelText: 'Description'),
        controller: _.descriptionCtrl,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _delayDes(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => TextFormField(
        keyboardType: TextInputType.multiline,
        maxLines: 10,
        decoration: InputDecoration(labelText: 'Descripcion de retraso'),
        controller: _.delayDescCtrl,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _phase(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => DropDownFormField(
        filled: false,
        titleText: 'Fase',
        value: _.phaseCtrl,
        onChanged: (value) {
          _.phaseCtrl = value;
          _.update(['add-task']);
        },
        hintText: 'Seleciona uno',
        dataSource: _.dataPhase,
        textField: 'display',
        valueField: 'value',
      ),
    );
  }

  Widget _start(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => TextFormField(
          readOnly: true,
          showCursor: true,
          decoration: InputDecoration(labelText: 'Inicio'),
          controller: _.startCtrl,
          style:
              TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
          validator: (value) {
            if (value.isEmpty) {
              return 'El campo es requerido';
            }
            return null;
          },
          onTap: () {
            DatePicker.showDatePicker(
              context,
              showTitleActions: true,
              minTime: DateTime(2021, 1, 1),
              maxTime: DateTime(2024, 6, 7),
              onConfirm: (date) {
                _.startCtrl.text = '${date.year}-${date.month}-${date.day}';
                _.update(['formDialog']);
              },
              currentTime: DateTime.now(),
              locale: LocaleType.es,
            );
          }),
    );
  }

  Widget _end(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => TextFormField(
          readOnly: true,
          showCursor: true,
          decoration: InputDecoration(labelText: 'Fin'),
          controller: _.endCtrl,
          style:
              TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
          validator: (value) {
            if (value.isEmpty) {
              return 'El campo es requerido';
            }
            return null;
          },
          onTap: () {
            DatePicker.showDatePicker(
              context,
              showTitleActions: true,
              minTime: DateTime(2021, 1, 1),
              maxTime: DateTime(2024, 6, 7),
              onConfirm: (date) {
                _.endCtrl.text = '${date.year}-${date.month}-${date.day}';
                _.update(['formDialog']);
              },
              currentTime: DateTime.now(),
              locale: LocaleType.es,
            );
          }),
    );
  }

  Widget _hours(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => TextFormField(
        readOnly: true,
        decoration: InputDecoration(labelText: 'Horas Planeadas'),
        controller: _.hoursCtrl,
        keyboardType: TextInputType.number,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _hoursWorked(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => TextFormField(
        decoration: InputDecoration(labelText: 'Hours Trabajadas'),
        controller: _.hoursWorkedCtrl,
        keyboardType: TextInputType.number,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty || value == null) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _taskPlanned(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => Row(
        children: [
          Text('Tarea No Planeada'),
          Checkbox(
            checkColor: Colors.greenAccent,
            activeColor: Colors.red,
            value: _.taskNoPlannedCtrl,
            onChanged: (bool value) {
              _.update(['add-task']);
              _.taskNoPlannedCtrl = value;
            },
          ),
        ],
      ),
    );
  }

  Widget _status(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      builder: (_) => DropDownFormField(
        titleText: 'Status',
        value: _.statusCtrl,
        onChanged: (value) {
          _.statusCtrl = value;
          _.update(['add-task']);
        },
        hintText: 'Seleciona uno',
        dataSource: _.dataStatus,
        textField: 'display',
        valueField: 'value',
      ),
    );
  }

  Widget _submit(BuildContext context) {
    return GetBuilder<UpdateTaskController>(
      init: UpdateTaskController(),
      builder: (_) {
        return ElevatedButton.icon(
          style: ElevatedButton.styleFrom(
            minimumSize: Size(w * 0.9, h * 0.07),
            primary: Colors.blue[500],
            onPrimary: Colors.white,
            onSurface: Colors.grey,
          ),
          onPressed: () async {
            if (_.formAddTask.currentState.validate()) {
              _loadingDialog(context);

              final response = await _.updateTask();

              Navigator.of(context).pop();

              await Get.dialog(
                DefaultMessage(
                  titulo: response['type'],
                  mensaje: response['msg'],
                ),
              );
              if (response['status']) {
                Get.back();
              }
            }
          },
          icon: FaIcon(FontAwesomeIcons.save),
          label: Text('Guardar'),
        );
      },
    );
  }

  Future<void> _loadingDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return GetBuilder<UpdateTaskController>(
          id: 'loadingDialog',
          builder: (_) {
            return AlertDialog(
              content: Container(
                height: MediaQuery.of(context).size.height * 0.15,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
