import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/data/provider/task_provider.dart';
import 'package:nd_app/app/data/repository/account_preferences.dart';

class AddTaskController extends GetxController {
  final TaskProvider _taskProvider = TaskProvider();
  final AccountPreferences _accountPreferences = AccountPreferences();

  final formAddTask = GlobalKey<FormState>();
  bool _updateForm;
  bool _loading;

  String projectCtrl;
  List dataProject = [
    {
      'display': 'None',
      'value': '0',
    }
  ];
  final TextEditingController activityCtrl = TextEditingController();
  final TextEditingController descriptionCtrl = TextEditingController();
  String phaseCtrl;
  List dataPhase = [
    {
      'display': 'None',
      'value': '0',
    }
  ];
  final TextEditingController startCtrl = TextEditingController();
  final TextEditingController endCtrl = TextEditingController();
  final TextEditingController hoursCtrl = TextEditingController();
  String statusCtrl;
  List dataStatus = [
    {
      'display': 'None',
      'value': '0',
    }
  ];
  String taskId = '';

  bool get loading => _loading;
  bool get updateForm => _updateForm;
  set updateForm(bool val) => this._updateForm = val;

  @override
  void onInit() {
    _updateForm = false;
    _loading = true;
    _initForm();
    super.onInit();
  }

  _initForm() async {
    await _getProjects();
    await _getPhases();
    await _getStatus();
    _loading = false;
    update(['add-task']);
  }

  Future<bool> _getProjects() async {
    final res = await _taskProvider.getProjects();
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);
      for (var item in dataMap['data']) {
        dataProject.add({
          'value': item['_id'],
          'display': item['name'],
        });
      }
    }
    return true;
  }

  Future<bool> _getPhases() async {
    final res = await _taskProvider.getPhases();
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);
      for (var item in dataMap['data']) {
        dataPhase.add({
          'value': item['_id'],
          'display': item['description'],
        });
      }
    }
    return true;
  }

  Future<bool> _getStatus() async {
    final res = await _taskProvider.getStatus();
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);
      for (var item in dataMap['data']) {
        dataStatus.add({
          'value': item['_id'],
          'display': item['description'],
        });
      }
    }
    return true;
  }

  createTask() async {
    Map<String, dynamic> body = {
      'projectId': projectCtrl,
      'activity': activityCtrl.text,
      'description': descriptionCtrl.text,
      'phase': phaseCtrl,
      'start': startCtrl.text,
      'end': endCtrl.text,
      'hours': hoursCtrl.text,
      'status': statusCtrl,
      'userId': _accountPreferences.id
    };
    final res = await _taskProvider.createTask(body);
    Map<String, dynamic> response = {
      'type': 'Error',
      'msg': '',
      'status': false
    };
    if (res.statusCode == 201) {
      response['type'] = 'Success';
      response['status'] = true;
    }
    Map dataMap = json.decode(res.body);
    response['msg'] = dataMap['message'];
    return response;
  }
}
