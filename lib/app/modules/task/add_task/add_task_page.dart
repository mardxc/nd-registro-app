import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/modules/task/add_task/add_task_controller.dart';
import 'package:nd_app/app/widgets/default_dialog.dart';
import 'package:nd_app/app/widgets/wait_dialog.dart';

class AddTaskPage extends StatelessWidget {
  final double h = Get.size.height;
  final double w = Get.size.width;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Agregar Tarea'),
      ),
      body: GetBuilder<AddTaskController>(
        id: 'add-task',
        init: AddTaskController(),
        builder: (_) {
          Widget widget = Container();
          if (_.loading) {
            widget = WaitDialog();
          } else {
            widget = Container(
              margin: EdgeInsets.symmetric(
                horizontal: w * 0.04,
                vertical: h * 0.04,
              ),
              width: w * 0.96,
              child: SingleChildScrollView(
                child: Form(
                  key: _.formAddTask,
                  child: Column(
                    children: [
                      _project(context),
                      _activity(context),
                      _description(context),
                      _phase(context),
                      _start(context),
                      _end(context),
                      _hours(context),
                      _status(context),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.03),
                      _submit(context),
                    ],
                  ),
                ),
              ),
            );
          }
          return widget;
        },
      ),
    );
  }

  Widget _project(BuildContext context) {
    return GetBuilder<AddTaskController>(
      builder: (_) => DropDownFormField(
        titleText: 'Proyecto',
        value: _.projectCtrl,
        onChanged: (value) {
          _.projectCtrl = value;
          _.update(['add-task']);
        },
        hintText: 'Seleciona uno',
        dataSource: _.dataProject,
        textField: 'display',
        valueField: 'value',
      ),
    );
  }

  Widget _activity(BuildContext context) {
    return GetBuilder<AddTaskController>(
      builder: (_) => TextFormField(
        decoration: InputDecoration(labelText: 'Actividad'),
        controller: _.activityCtrl,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _description(BuildContext context) {
    return GetBuilder<AddTaskController>(
      builder: (_) => TextFormField(
        decoration: InputDecoration(labelText: 'Descripcion'),
        controller: _.descriptionCtrl,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _phase(BuildContext context) {
    return GetBuilder<AddTaskController>(
      builder: (_) => DropDownFormField(
        titleText: 'Fase',
        value: _.phaseCtrl,
        onChanged: (value) {
          _.phaseCtrl = value;
          _.update(['add-task']);
        },
        hintText: 'Seleciona uno',
        dataSource: _.dataPhase,
        textField: 'display',
        valueField: 'value',
      ),
    );
  }

  Widget _start(BuildContext context) {
    return GetBuilder<AddTaskController>(
      builder: (_) => TextFormField(
          readOnly: true,
          showCursor: true,
          decoration: InputDecoration(labelText: 'Inicio'),
          controller: _.startCtrl,
          style:
              TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
          validator: (value) {
            if (value.isEmpty) {
              return 'El campo es requerido';
            }
            return null;
          },
          onTap: () {
            DatePicker.showDatePicker(
              context,
              showTitleActions: true,
              minTime: DateTime(2021, 1, 1),
              maxTime: DateTime(2024, 6, 7),
              onConfirm: (date) {
                _.startCtrl.text = '${date.year}-${date.month}-${date.day}';
                _.update(['formDialog']);
              },
              currentTime: DateTime.now(),
              locale: LocaleType.es,
            );
          }),
    );
  }

  Widget _end(BuildContext context) {
    return GetBuilder<AddTaskController>(
      builder: (_) => TextFormField(
          readOnly: true,
          showCursor: true,
          decoration: InputDecoration(labelText: 'Fin'),
          controller: _.endCtrl,
          style:
              TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
          validator: (value) {
            if (value.isEmpty) {
              return 'El campo es requerido';
            }
            return null;
          },
          onTap: () {
            DatePicker.showDatePicker(
              context,
              showTitleActions: true,
              minTime: DateTime(2021, 1, 1),
              maxTime: DateTime(2024, 6, 7),
              onConfirm: (date) {
                _.endCtrl.text = '${date.year}-${date.month}-${date.day}';
                _.update(['formDialog']);
              },
              currentTime: DateTime.now(),
              locale: LocaleType.es,
            );
          }),
    );
  }

  Widget _hours(BuildContext context) {
    return GetBuilder<AddTaskController>(
      builder: (_) => TextFormField(
        decoration: InputDecoration(labelText: 'Horas Planeadas'),
        controller: _.hoursCtrl,
        keyboardType: TextInputType.number,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _status(BuildContext context) {
    return GetBuilder<AddTaskController>(
      builder: (_) => DropDownFormField(
        titleText: 'Status',
        value: _.statusCtrl,
        onChanged: (value) {
          _.statusCtrl = value;
          _.update(['add-task']);
        },
        hintText: 'Seleciona uno',
        dataSource: _.dataStatus,
        textField: 'display',
        valueField: 'value',
      ),
    );
  }

  Widget _submit(BuildContext context) {
    return GetBuilder<AddTaskController>(
      init: AddTaskController(),
      builder: (_) {
        return ElevatedButton.icon(
          style: ElevatedButton.styleFrom(
            minimumSize: Size(w, h * 0.07),
            primary: Colors.blue[500],
            onPrimary: Colors.white,
            onSurface: Colors.grey,
          ),
          onPressed: () async {
            if (_.formAddTask.currentState.validate()) {
              _loadingDialog(context);

              final response = await _.createTask();

              Navigator.of(context).pop();

              await Get.dialog(
                DefaultMessage(
                  titulo: response['type'],
                  mensaje: response['msg'],
                ),
              );
              if (response['status']) {
                Get.back();
              }
            }
          },
          icon: FaIcon(FontAwesomeIcons.save),
          label: Text('Save'),
        );
      },
    );
  }

  Future<void> _loadingDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return GetBuilder<AddTaskController>(
          id: 'loadingDialog',
          builder: (_) {
            return AlertDialog(
              content: Container(
                height: MediaQuery.of(context).size.height * 0.15,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
