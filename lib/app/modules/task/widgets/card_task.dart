import 'package:flutter/material.dart';

class CardTask extends StatelessWidget {
  final tasks;
  final index;
  const CardTask({this.tasks, this.index});

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    return Container(
      child: Card(
        elevation: 3,
        child: ListTile(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Proyecto ${tasks.project}',
                style:
                    TextStyle(fontSize: h * 0.02, fontWeight: FontWeight.bold),
              ),
              Text(
                'Actividad ${tasks.activity}',
                style:
                    TextStyle(fontSize: h * 0.015, fontWeight: FontWeight.bold),
              ),
            ],
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Inicio ${tasks.start}',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Text(
                    'Fin ${tasks.end}',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Horas ${tasks.hours}',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Text(
                    'Status ${tasks.status}',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
