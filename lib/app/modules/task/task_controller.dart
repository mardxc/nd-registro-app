import 'package:get/get.dart';
import 'package:inline_calender/inline_calender.dart';
import 'package:nd_app/app/data/model/task_model.dart';
import 'package:nd_app/app/data/provider/task_provider.dart';
import 'package:nd_app/app/data/repository/account_preferences.dart';

class TaskController extends GetxController {
  TaskProvider _taskProvider = TaskProvider();
  final AccountPreferences accountPreferences = AccountPreferences();
  InlineCalenderModel _calenderCtrl;
  DateTime _pickedDate = DateTime.now();
  bool _loading;

  List<TaskModel> _task;

  InlineCalenderModel get calenderCtrl => _calenderCtrl;
  bool get loading => _loading;

  DateTime get pickedDate => _pickedDate;
  List<TaskModel> get tasks => _task;

  set pickedDate(DateTime val) => this._pickedDate = val;

  @override
  void onInit() {
    _loading = true;

    _calenderCtrl = InlineCalenderModel(
      defaultSelectedDate: _pickedDate,
      onChange: (DateTime date) {
        loadTask(date);
        _pickedDate = date;
      },
    );
    accountPreferences.initPrefs();
    super.onInit();
  }

  @override
  void onReady() {
    loadTask(_pickedDate);
    super.onReady();
  }

  @override
  void onClose() {
    _calenderCtrl.dispose();

    super.onClose();
  }

  Future<void> loadTask(date) async {
    final today = '${date.year}-${date.month}-${date.day}';
    this._loading = true;
    update(['tasks_list']);
    final data = await _taskProvider.getTasks(today);
    this._task = data;
    this._loading = false;
    update(['tasks_list']);
  }
}
