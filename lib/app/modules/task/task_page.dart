import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inline_calender/inline_calender.dart';
import 'package:nd_app/app/modules/drawer/drawer_page.dart';
import 'package:nd_app/app/modules/task/task_controller.dart';
import 'package:nd_app/app/modules/task/widgets/card_task.dart';
import 'package:nd_app/app/modules/task/widgets/no_card_task.dart';
import 'package:nd_app/app/widgets/app_background.dart';
import 'package:nd_app/app/widgets/wait_dialog.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class TaskPage extends StatelessWidget {
  final double h = Get.size.height;
  final double w = Get.size.width;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TaskController>(
      init: TaskController(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              'Tareas',
              style: TextStyle(color: Colors.white),
            ),
            bottom: InlineCalender(
                controller: _.calenderCtrl,
                locale: Locale('es_MX'),
                isShamsi: false,
                height: 100,
                maxWeeks: 12,
                middleWeekday: DateTime.now().weekday),
          ),
          drawer: DrawerPage(),
          extendBodyBehindAppBar: true,
          body: Scaffold(
            backgroundColor: Colors.black,
            body: AppBackground(
              child: _listaCard(),
            ),
            floatingActionButton: SpeedDial(
              icon: Icons.add,
              backgroundColor: Theme.of(context).primaryColor,
              overlayColor: Theme.of(context).primaryColorLight,
              overlayOpacity: 0.3,
              children: _speedDialButtons(context, _),
            ),
          ),
        );
      },
    );
  }

  List<SpeedDialChild> _speedDialButtons(
      BuildContext context, TaskController _) {
    List<SpeedDialChild> widget = [
      SpeedDialChild(
        child: FaIcon(FontAwesomeIcons.userPlus),
        label: 'Asignar tarea',
        elevation: 10,
        backgroundColor: Theme.of(context).accentColor,
        onTap: () async {
          await Get.toNamed('/assign-task');
          _.loadTask(_.pickedDate);
        },
      )
    ];

    if (_.accountPreferences.role != 'colaborator') {
      widget.add(
        SpeedDialChild(
          child: FaIcon(FontAwesomeIcons.pencilAlt),
          label: 'Nueva tarea',
          elevation: 10,
          backgroundColor: Theme.of(context).accentColor,
          onTap: () async {
            await Get.toNamed('/add-task');
            _.loadTask(_.pickedDate);
          },
        ),
      );
    }
    return widget;
  }

  Widget _listaCard() {
    return GetBuilder<TaskController>(
      id: 'tasks_list',
      builder: (_) {
        if (_.loading) {
          return WaitDialog();
        } else {
          return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: _.tasks.length > 1 ? _.tasks.length : 1,
            itemBuilder: (context, index) {
              if (_.tasks.length != 0) {
                return GestureDetector(
                  onTap: () async {
                    if (_.tasks[index].status != 'Finalizada') {
                      await Get.toNamed('/update-task/${_.tasks[index].id}');
                      _.loadTask(_.pickedDate);
                    }
                  },
                  child: CardTask(tasks: _.tasks[index], index: index),
                );
              } else {
                return NoCardTask();
              }
            },
          );
        }
      },
    );
  }
}
