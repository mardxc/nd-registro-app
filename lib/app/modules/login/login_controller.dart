import 'dart:convert';

import 'package:nd_app/app/data/provider/login_provider.dart';
import 'package:nd_app/app/data/repository/account_preferences.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final AccountPreferences _accountPreferences = AccountPreferences();
  final formKeyLogin = GlobalKey<FormState>();
  final LoginProvider _loginProvider = LoginProvider();

  @override
  void onInit() {
    _accountPreferences.initPrefs();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  doLogin() async {
    Map res = {
      'msg': 'No se completo la solicitud',
      'type': '',
      'login': false
    };
    try {
      Map body = {
        'password': passController.text,
        'email': emailController.text
      };

      final response = await _loginProvider.getAccessLogin(body);
      Map dataMap = json.decode(response.body);
      if (response.statusCode == 200) {
        _accountPreferences.token = dataMap['token'];
        res['login'] = true;
      }
      res['msg'] = dataMap['message'];
    } catch (e) {
      res['msg'] = e.toString;
      res['type'] = 'Error';
    }
    return res;
  }
}
