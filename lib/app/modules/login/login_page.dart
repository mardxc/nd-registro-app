import 'dart:ui';

import 'package:nd_app/app/modules/login/login_controller.dart';
import 'package:nd_app/app/modules/login/widgets/form_widget.dart';
import 'package:nd_app/app/modules/login/widgets/buttons_widget.dart';
import 'package:nd_app/app/widgets/default_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends GetView<LoginController> {
  final double h = Get.size.height;
  final double w = Get.size.width;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      extendBodyBehindAppBar: true,
      body: GetBuilder<LoginController>(
        init: LoginController(),
        builder: (_) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/background/login-background.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: _contenido(context),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _contenido(BuildContext context) {
    return GetBuilder<LoginController>(
      builder: (_) => SafeArea(
        child: Column(
          children: [
            Image.asset(
              'assets/img/nd-logo.png',
              height: h * 0.3,
            ),
            FractionallySizedBox(
              widthFactor: 0.9,
              child: FormWidget(),
            ),
            SizedBox(height: h * 0.01),
            Text.rich(
              TextSpan(text: '¿Olvidaste tu contraseña?'),
              style: TextStyle(
                fontFamily: 'Merriweather',
                fontStyle: FontStyle.italic,
              ),
              maxLines: 2,
            ),
            SizedBox(height: h * 0.01),
            _submit(context),
            SizedBox(height: h * 0.01),
            Stack(
              alignment: Alignment.center,
              children: [
                Divider(
                  indent: w * 0.2,
                  endIndent: w * 0.6,
                  color: Colors.white,
                ),
                Text.rich(
                  TextSpan(text: 'OR'),
                  style: TextStyle(color: Colors.white),
                ),
                Divider(
                  indent: w * 0.6,
                  endIndent: w * 0.2,
                  color: Colors.white,
                ),
              ],
            ),
            SizedBox(height: h * 0.01),
            ButtonsWidget(),
          ],
        ),
      ),
    );
  }

  Widget _submit(BuildContext context) {
    return GetBuilder<LoginController>(
      builder: (_) => ElevatedButton(
        style: ElevatedButton.styleFrom(
          minimumSize: Size(w * 0.8, h * 0.07),
          primary: Theme.of(context).primaryColor,
          onPrimary: Colors.white,
          onSurface: Colors.grey,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
        onPressed: () async {
          if (_.formKeyLogin.currentState.validate()) {
            _loadingDialog(context);
            final response = await _.doLogin();
            Navigator.of(context).pop();
            await Get.dialog(
              DefaultMessage(
                titulo: response['type'],
                mensaje: response['msg'],
              ),
            );
            if (response['login']) {
              Get.offAllNamed('/splash');
            }
          }
        },
        child: Text('Iniciar sesion'),
      ),
    );
  }

  Future<void> _loadingDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return GetBuilder<LoginController>(
          id: 'loadingDialog',
          builder: (_) {
            return AlertDialog(
              content: Container(
                height: MediaQuery.of(context).size.height * 0.15,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
