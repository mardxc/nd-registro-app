import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ButtonsWidget extends StatelessWidget {
  const ButtonsWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double h = MediaQuery.of(context).size.height;
    final double w = MediaQuery.of(context).size.width;
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        minimumSize: Size(w * 0.8, h * 0.07),
        primary: Colors.blue[500],
        onPrimary: Colors.white,
        onSurface: Colors.grey,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      onPressed: () {
        Get.toNamed('/signup');
      },
      icon: Icon(Icons.mail_outline_rounded),
      label: Text('Sign Up'),
    );
  }
}
