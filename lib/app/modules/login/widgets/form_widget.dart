import 'package:nd_app/app/modules/login/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FormWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
      builder: (_) => Card(
        elevation: 0,
        color: Colors.white.withOpacity(0.5),
        child: Form(
          key: _.formKeyLogin,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: Column(
              children: [
                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                _email(context),
                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                _password(context),
                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _email(BuildContext context) {
    return GetBuilder<LoginController>(
      builder: (_) => TextFormField(
        controller: _.emailController,
        decoration: InputDecoration(
          labelText: 'Email',
        ),
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty || !value.contains("@")) {
            return 'Please enter some email';
          }
          return null;
        },
      ),
    );
  }

  Widget _password(BuildContext context) {
    return GetBuilder<LoginController>(
      builder: (_) => TextFormField(
        decoration: InputDecoration(labelText: 'Password'),
        controller: _.passController,
        obscureText: true,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'Please enter some password';
          }
          return null;
        },
      ),
    );
  }
}
