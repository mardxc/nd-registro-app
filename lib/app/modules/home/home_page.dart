import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:inline_calender/inline_calender.dart';
import 'package:nd_app/app/modules/drawer/drawer_page.dart';
import 'package:nd_app/app/modules/home/home_controller.dart';
import 'package:nd_app/app/modules/home/widgets/card_workday.dart';
import 'package:nd_app/app/modules/home/widgets/no_card_workday.dart';
import 'package:nd_app/app/widgets/app_background.dart';
import 'package:nd_app/app/widgets/default_dialog.dart';
import 'package:nd_app/app/widgets/wait_dialog.dart';

class HomePage extends StatelessWidget {
  final double h = Get.size.height;
  final double w = Get.size.width;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              'Home',
              style: TextStyle(color: Colors.white),
            ),
            bottom: InlineCalender(
                controller: _.calenderCtrl,
                locale: Locale('es_MX'),
                isShamsi: false,
                height: 100,
                maxWeeks: 12,
                middleWeekday: DateTime.now().weekday),
          ),
          drawer: DrawerPage(),
          extendBodyBehindAppBar: true,
          body: Scaffold(
            backgroundColor: Colors.black,
            body: AppBackground(
              child: _listaCard(),
            ), //_contenido(context),),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => _addActivity(context),
            child: Icon(Icons.add),
          ),
        );
      },
    );
  }

  Widget _listaCard() {
    return GetBuilder<HomeController>(
      id: 'workdays_list',
      builder: (_) {
        if (_.loading) {
          return WaitDialog();
        } else {
          return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: _.workdays.length > 1 ? _.workdays.length : 1,
            itemBuilder: (context, index) {
              if (_.workdays.length != 0) {
                return CardWorkday(workdays: _.workdays[index], index: index);
              } else {
                return NoCardWorkday();
              }
            },
          );
        }
      },
    );
  }

  Future<void> _addActivity(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return GetBuilder<HomeController>(
          id: 'add-activity',
          builder: (_) {
            return AlertDialog(
              scrollable: true,
              content: Form(
                child: Column(
                  children: [
                    DropDownFormField(
                      titleText: 'Actividad',
                      value: _.activityCtrl,
                      onChanged: (value) {
                        _.activityCtrl = value;
                        _.update(['add-activity']);
                      },
                      hintText: 'Seleciona uno',
                      dataSource: _.dataActivity,
                      textField: 'display',
                      valueField: 'value',
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                    ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(w, h * 0.07),
                        primary: Colors.blue[500],
                        onPrimary: Colors.white,
                        onSurface: Colors.grey,
                      ),
                      onPressed: () async {
                        _loadingDialog(context);

                        final response = await _.checkIn();

                        Navigator.of(context).pop();

                        await Get.dialog(
                          DefaultMessage(
                            titulo: 'ND',
                            mensaje: response,
                          ),
                        );
                        Navigator.of(context).pop();
                      },
                      icon: FaIcon(FontAwesomeIcons.save),
                      label: Text('Save'),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  Future<void> _loadingDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return GetBuilder<HomeController>(
          id: 'loadingDialog',
          builder: (_) {
            return AlertDialog(
              content: Container(
                height: MediaQuery.of(context).size.height * 0.15,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
