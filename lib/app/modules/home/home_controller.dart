import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:geolocator/geolocator.dart';
import 'package:inline_calender/inline_calender.dart';

import 'package:nd_app/app/data/model/wokdays_model.dart';
import 'package:nd_app/app/data/provider/home_provider.dart';
import 'package:nd_app/app/data/repository/account_preferences.dart';

class HomeController extends GetxController {
  HomeProvider _homeProvider = HomeProvider();
  final AccountPreferences _accountPreferences = AccountPreferences();
  InlineCalenderModel _calenderCtrl;
  DateTime _pickedDate = DateTime.now();
  final formAddActivity = GlobalKey<FormState>();

  String activityCtrl;
  List dataActivity = [
    {
      'display': 'None',
      'value': '0',
    }
  ];

  InlineCalenderModel get calenderCtrl => _calenderCtrl;
  bool _loading;
  List<WorkdaysModel> _workdays;
  int _check;

  @override
  void onInit() {
    _getActivities();
    _loading = true;
    _calenderCtrl = InlineCalenderModel(
      defaultSelectedDate: _pickedDate,
      onChange: (DateTime date) =>
          loadWorkddays('${date.year}-${date.month}-${date.day}'),
    );
    _workdays = [];
    _accountPreferences.initPrefs();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  int get check => _check;
  bool get loading => _loading;
  AccountPreferences get preff => _accountPreferences;
  List<WorkdaysModel> get workdays => _workdays;

  Future<bool> _getActivities() async {
    final res = await _homeProvider.getActivities();
    if (res.statusCode == 200) {
      Map dataMap = json.decode(res.body);
      for (var item in dataMap['data']) {
        dataActivity.add({
          'value': item['_id'],
          'display': item['description'],
        });
      }
      dataActivity.removeAt(0);
    }
    return true;
  }

  Future<void> loadWorkddays(date) async {
    this._loading = true;
    update(['workdays_list']);
    final data = await _homeProvider.getWorkdays(date);
    this._workdays = data;
    this._loading = false;
    update(['workdays_list']);
  }

  checkIn() async {
    String msg = '';

    try {
      final position = await _determinePosition();
      Map<String, dynamic> body = {
        'lat': position.latitude,
        'lon': position.longitude,
        'activity': activityCtrl,
      };

      final response = await _homeProvider.checkIn(body);
      Map decodeMap = json.decode(response.body);
      msg = decodeMap['message'];
    } catch (e) {
      msg = e.toString();
    }

    loadWorkddays(
        '${_pickedDate.year}-${_pickedDate.month}-${_pickedDate.day}');
    return msg;
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best,
        forceAndroidLocationManager: true);
  }
}
