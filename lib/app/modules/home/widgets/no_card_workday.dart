import 'package:flutter/material.dart';

class NoCardWorkday extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Container(
      width: w,
      height: h * 0.12,
      child: Card(
        elevation: 10,
        child: Text(
          'Sin registros',
          style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
