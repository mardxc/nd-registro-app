import 'package:flutter/material.dart';
import 'package:nd_app/app/utils/validations.dart';

class CardWorkday extends StatelessWidget {
  final workdays;
  final index;
  const CardWorkday({this.workdays, this.index});

  @override
  Widget build(BuildContext context) {
    final Validate _validar = Validate();
    double h = MediaQuery.of(context).size.height;
    return Container(
      height: h * 0.1,
      child: Card(
        elevation: 3,
        child: ListTile(
          leading: Image.network(
            'https://picsum.photos/250?image=9',
            fit: BoxFit.fill,
          ),
          title: Text(
            '${workdays.activity}',
            style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            'Hora ${_validar.convertirFechaToHora(workdays.start)}',
            style: TextStyle(
              fontWeight: FontWeight.normal,
            ),
          ),
          onTap: () {
            print("taped");
          },
        ),
      ),
    );
  }
}
