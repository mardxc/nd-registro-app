import 'package:flutter/material.dart';

class CardWorkdayMock extends StatelessWidget {
  final titulo;
  final entrada;
  final salida;
  const CardWorkdayMock({this.titulo, this.entrada, this.salida});

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    return Container(
      height: h * 0.12,
      child: Card(
        elevation: 3,
        child: ListTile(
          leading: Image.network(
            'https://picsum.photos/250?image=9',
            fit: BoxFit.fill,
          ),
          title: Text(
            '$titulo',
            style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            'Hora $entrada',
            style: TextStyle(
              fontWeight: FontWeight.normal,
            ),
          ),
          onTap: () {
            print("taped");
          },
        ),
      ),
    );
  }
}
