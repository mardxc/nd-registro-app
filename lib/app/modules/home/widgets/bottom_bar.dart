import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/modules/home/home_controller.dart';
import 'package:nd_app/app/widgets/default_dialog.dart';

class BottomBar extends StatefulWidget {
  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  final double h = Get.size.height;
  final double w = Get.size.width;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              minimumSize: Size(w * 0.5, h * 0.07),
              primary: Colors.green[500],
              onPrimary: Colors.white,
              onSurface: Colors.grey,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1),
              ),
            ),
            onPressed: () async {
              if (_.preff.check == 0) {
                _loadingDialog(context);

                final response = await _.checkIn();
                Navigator.of(context).pop();

                await Get.dialog(
                  DefaultMessage(
                    titulo: '',
                    mensaje: response,
                  ),
                );
              }
            },
            child: Text('Entrada'),
          ),
          Text(''),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              minimumSize: Size(w * 0.5, h * 0.07),
              primary: Colors.red[500],
              onPrimary: Colors.white,
              onSurface: Colors.grey,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1),
              ),
            ),
            onPressed: () async {
              if (_.preff.check == 1) {
                _loadingDialog(context);

                final response = await _.checkIn();
                Navigator.of(context).pop();

                await Get.dialog(
                  DefaultMessage(
                    titulo: '',
                    mensaje: response,
                  ),
                );
              }
            },
            child: Text('Salida'),
          ),
        ],
      ),
    );
  }

  Future<void> _loadingDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return GetBuilder<HomeController>(
          id: 'loadingDialog',
          builder: (_) {
            return AlertDialog(
              content: Container(
                height: MediaQuery.of(context).size.height * 0.15,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
