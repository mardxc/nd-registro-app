import 'package:get/get.dart';
import 'package:nd_app/app/data/repository/account_preferences.dart';

class DrawerHomeController extends GetxController {
  final AccountPreferences _accountPreferences = AccountPreferences();

  @override
  void onInit() {
    _accountPreferences.initPrefs();
    super.onInit();
  }

  void logout() {
    _accountPreferences.token = '';
    Get.offAllNamed('/login');
  }
}
