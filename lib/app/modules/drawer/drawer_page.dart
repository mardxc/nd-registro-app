import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/modules/drawer/drawer_controller.dart';

class DrawerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DrawerHomeController>(
      init: DrawerHomeController(),
      builder: (_) => Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text(''),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image:
                      AssetImage('assets/img/background/login-background.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            ListTile(
              title: Text('Home'),
              onTap: () {
                Get.offAllNamed('/home');
              },
            ),
            ListTile(
              title: Text('Tareas'),
              onTap: () {
                Get.offAllNamed('/task');
              },
            ),
            ListTile(
              title: Text('Cerrar Sesion'),
              onTap: () {
                _.logout();
              },
            ),
          ],
        ),
      ),
    );
  }
}
