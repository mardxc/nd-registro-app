import 'package:nd_app/app/modules/signup/widgets/form_widget.dart';
import 'package:nd_app/app/modules/signup/signup_controller.dart';
import 'package:nd_app/app/modules/signup/widgets/background_widget.dart';
import 'package:nd_app/app/modules/signup/widgets/buttons_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignupPage extends GetView<SignupController> {
  final double h = Get.size.height;
  final double w = Get.size.width;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      extendBodyBehindAppBar: true,
      body: GetBuilder<SignupController>(
        init: SignupController(),
        builder: (_) {
          return Scaffold(
            body: BackgroundWidget(
              child: Container(
                color: Colors.blue.withOpacity(0.1),
                child: SafeArea(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05),
                        FractionallySizedBox(
                          widthFactor: 0.8,
                          child: FormWidget(),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.03),
                        ButtonsWidget(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
