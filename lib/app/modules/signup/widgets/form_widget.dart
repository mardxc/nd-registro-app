import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nd_app/app/modules/signup/signup_controller.dart';

class FormWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SignupController>(
      builder: (_) => Card(
        elevation: 0,
        color: Colors.white.withOpacity(0.5),
        child: Form(
          key: _.formKeySignup,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: Column(
              children: [
                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                _name(context),
                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                _lastname(context),
                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                _email(context),
                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                _username(context),
                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                _password(context),
                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _email(BuildContext context) {
    return GetBuilder<SignupController>(
      builder: (_) => TextFormField(
        controller: _.emailController,
        decoration: InputDecoration(
          labelText: 'Email',
        ),
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty || !value.contains("@")) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _password(BuildContext context) {
    return GetBuilder<SignupController>(
      builder: (_) => TextFormField(
        decoration: InputDecoration(labelText: 'Password'),
        controller: _.passController,
        obscureText: true,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _name(BuildContext context) {
    return GetBuilder<SignupController>(
      builder: (_) => TextFormField(
        decoration: InputDecoration(labelText: 'Nombre'),
        controller: _.nameController,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _lastname(BuildContext context) {
    return GetBuilder<SignupController>(
      builder: (_) => TextFormField(
        decoration: InputDecoration(labelText: 'Apellido'),
        controller: _.lastnameController,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }

  Widget _username(BuildContext context) {
    return GetBuilder<SignupController>(
      builder: (_) => TextFormField(
        decoration: InputDecoration(labelText: 'Username'),
        controller: _.usernameController,
        style: TextStyle(fontSize: MediaQuery.of(context).size.height * 0.021),
        validator: (value) {
          if (value.isEmpty) {
            return 'El campo es requerido';
          }
          return null;
        },
      ),
    );
  }
}
