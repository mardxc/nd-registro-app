import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:nd_app/app/modules/signup/signup_controller.dart';
import 'package:nd_app/app/widgets/default_dialog.dart';

class ButtonsWidget extends StatelessWidget {
  const ButtonsWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double h = MediaQuery.of(context).size.height;
    final double w = MediaQuery.of(context).size.width;
    return GetBuilder<SignupController>(
      builder: (_) => Column(
        children: [
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              minimumSize: Size(w * 0.8, h * 0.07),
              primary: Theme.of(context).primaryColor,
              onPrimary: Colors.white,
              onSurface: Colors.grey,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
            onPressed: () async {
              if (_.formKeySignup.currentState.validate()) {
                _loadingDialog(context);

                final response = await _.doRegister();
                Navigator.of(context).pop();

                await Get.dialog(
                  DefaultMessage(
                    titulo: response['type'],
                    mensaje: response['msg'],
                  ),
                );

                if (response['login']) {
                  Get.offAllNamed('/splash');
                }
              }
            },
            icon: Icon(Icons.mail_outline_rounded),
            label: Text(
              'Registro',
            ),
          ),
          SizedBox(height: h * 0.01),
          Stack(
            alignment: Alignment.center,
            children: [
              Divider(
                indent: w * 0.2,
                endIndent: w * 0.6,
                color: Colors.white,
              ),
              Text.rich(
                TextSpan(text: 'OR'),
                style: TextStyle(color: Colors.white),
              ),
              Divider(
                indent: w * 0.6,
                endIndent: w * 0.2,
                color: Colors.white,
              ),
            ],
          ),
          SizedBox(height: h * 0.01),
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              minimumSize: Size(w * 0.8, h * 0.07),
              primary: Colors.blue[500],
              onPrimary: Colors.white,
              onSurface: Colors.grey,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
            onPressed: () {
              Get.offAllNamed('/login');
            },
            icon: FaIcon(FontAwesomeIcons.google),
            label: Text('Iniciar sesion'),
          ),
        ],
      ),
    );
  }

  Future<void> _loadingDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return GetBuilder<SignupController>(
          id: 'loadingDialog',
          builder: (_) {
            return AlertDialog(
              content: Container(
                height: MediaQuery.of(context).size.height * 0.15,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
