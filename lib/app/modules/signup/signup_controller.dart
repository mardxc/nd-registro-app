import 'package:get/get.dart';
import 'dart:convert';

import 'package:nd_app/app/data/provider/signup_provider.dart';
import 'package:nd_app/app/data/repository/account_preferences.dart';
import 'package:flutter/material.dart';

class SignupController extends GetxController {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController lastnameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final AccountPreferences _accountPreferences = AccountPreferences();
  final formKeySignup = GlobalKey<FormState>();
  final SignupProvider _signupProvider = SignupProvider();

  @override
  void onInit() {
    _accountPreferences.initPrefs();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  doRegister() async {
    Map res = {
      'msg': 'No se completo la solicitud',
      'type': '',
      'login': false
    };
    try {
      Map body = {
        'name': nameController.text,
        'lastname': lastnameController.text,
        'email': emailController.text,
        'username': usernameController.text,
        'password': passController.text
      };

      final response = await _signupProvider.doRegister(body);
      Map dataMap = json.decode(response.body);

      if (response.statusCode == 201) {
        _accountPreferences.token = dataMap['token'];
        res['login'] = true;
      }

      res['msg'] = dataMap['message'];
    } catch (e) {
      res['msg'] = e.toString;
      res['type'] = 'Error';
    }
    return res;
  }
}
