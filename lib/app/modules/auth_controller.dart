import 'dart:convert';

import 'package:get/get.dart';
import 'package:nd_app/app/data/provider/auth_provider.dart';
import 'package:nd_app/app/data/repository/account_preferences.dart';

class AuthController extends GetxController {
  final AccountPreferences _accountPreferences = AccountPreferences();
  final AuthProvider _authProvider = AuthProvider();

  @override
  void onInit() async {
    await _accountPreferences.initPrefs();
    super.onInit();
  }

  @override
  void onReady() {
    _checkLocalToken();
    super.onReady();
  }

  @override
  void onClose() {
    super.onReady();
  }

  _checkLocalToken() async {
    final response = await _authProvider.checkToken(_accountPreferences.token);
    Map dataMap = json.decode(response.body);
    if (response.statusCode == 200) {
      _accountPreferences.id = dataMap['data']['_id'];
      _accountPreferences.username = dataMap['data']['username'];
      _accountPreferences.email = dataMap['data']['email'];
      _accountPreferences.name = dataMap['data']['name'];
      _accountPreferences.lastname = dataMap['data']['lastname'];
      _accountPreferences.role = dataMap['data']['roles']['name'];

      Get.offAllNamed('/home');
    } else {
      _accountPreferences.token = '';
      Get.offAllNamed('/login');
    }
    update(['materialApp']);
  }
}
